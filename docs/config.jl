using FeaInterfaces, LinearAlgebra

package_info = Dict(
    "modules" => [FeaInterfaces],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "FeaInterfaces.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/FeaInterfaces",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(FeaInterfaces, :DocTestSetup, :(using FeaInterfaces); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(FeaInterfaces, fix=true)
end
