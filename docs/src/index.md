# FeaInterfaces.jl

[![pipeline status](https://gitlab.com/feather-ecosystem/FeaInterfaces/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/FeaInterfaces/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/FeaInterfaces/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/FeaInterfaces/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/FeaInterfaces/branch/master/graph/badge.svg?token=ESA77DBG7I)](https://codecov.io/gl/feather-ecosystem:core/FeaInterfaces)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/FeaInterfaces/)

***

This package implements fundamental finite element analysis interfaces, which are implemented in core packages in our [`Feather ecosystem`](https://gitlab.com/feather-ecosystem).

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/).