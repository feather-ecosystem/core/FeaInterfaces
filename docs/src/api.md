# API

```@docs
FeaInterfaces.@evaluate!
FeaInterfaces.Degree
FeaInterfaces.Dimension
FeaInterfaces.FeatherMapping
FeaInterfaces.Lazy
FeaInterfaces.Regularity
FeaInterfaces.boundary
FeaInterfaces.l2_error
LinearAlgebra.nullspace
```