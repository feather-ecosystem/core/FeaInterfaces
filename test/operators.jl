using Test, SafeTestsets

@safetestset "Operators" begin

    using FeaInterfaces
    import FeaInterfaces: Lazy, Hessian, Gradient, Divergence, Curl, Jacobian

    @testset "Constructors Lazy{Gradient}" begin
        f = Field{Function}((x,y,z) -> sin(x) * cos(y), (x,y,z) -> z*cos(x) * sin(y)^2)
        ∇f = gradient(f)
        @test ∇f isa Lazy{Gradient}
        @test size(∇f) == (2,3) && size(∇f,1) == 2 && size(∇f,2) == 3 && size(∇f,3) == 1
        @test ∇f[1,1] == ∇f[1] == (f[1], (1, 0, 0))
        @test ∇f[2,1] == ∇f[2] == (f[2], (1, 0, 0))
        @test ∇f[1,3] == ∇f[5] == (f[1], (0, 0, 1))
        @test ∇f[2,3] == ∇f[6] == (f[2], (0, 0, 1))
        @test_throws BoundsError ∇f[3,1]
        @test_throws BoundsError ∇f[7]
    end

    @testset "Constructors Lazy{Jacobian}" begin
        # general case
        f = Mapping{Function}((x,y,z) -> sin(x) * cos(y), (x,y,z) -> z*cos(x) * sin(y)^2)
        ∇f = jacobian(f)
        @test ∇f isa Lazy{Jacobian}
        @test size(∇f) == (2,3) && size(∇f,1) == 2 && size(∇f,2) == 3 && size(∇f,3) == 1
        @test ∇f[1,1] == ∇f[1] == (f[1], (1, 0, 0))
        @test ∇f[2,1] == ∇f[2] == (f[2], (1, 0, 0))
        @test ∇f[1,3] == ∇f[5] == (f[1], (0, 0, 1))
        @test ∇f[2,3] == ∇f[6] == (f[2], (0, 0, 1))
        @test_throws BoundsError ∇f[3,1]
        @test_throws BoundsError ∇f[7]
    end

    @testset "Differential operators on Fields" begin
        f = Field{Function}((x,y,z) -> sin(x) * cos(y))
        Δf = hessian(f)
        @test Δf isa Lazy{Hessian}
        @test size(Δf) == (3,3) && size(Δf,1) == 3 && size(Δf,2) == 3 && size(Δf,3) == 1
        @test Δf[1,1] == Δf[1] == (f[1], (2, 0, 0))
        @test Δf[2,1] == Δf[2] == (f[1], (1, 1, 0))
        @test Δf[1,3] == Δf[7] == (f[1], (1, 0, 1))
        @test Δf[2,3] == Δf[6] == (f[1], (0, 1, 1))
        @test_throws BoundsError Δf[4,1]
        @test_throws BoundsError Δf[10]
    end

end
