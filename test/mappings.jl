# SafeTestsets does not support macros. Hence, here we
# use a module to create a safe environment
module MappingsTests

using Test

using FeaInterfaces
import FeaInterfaces: nargs

@testset "Extract mapping arguments" begin
    g = (x,y,z) -> sin(x) * cos(y)
    @test nargs(g) == 3

    f = ((x,y) -> sin(x) * cos(y), (x,y) -> sin(x) * y, (x,y) -> sin(x) * y)
    @test dimension(Val(Function), f) == 2
    @test codimension(Val(Function), f) == 3
end

@testset "Construction of Fields" begin
    f = Field{Function}((x,y,z) -> sin(x) * cos(y))
    @test dimension(f) == 3
    @test codimension(f) == 1

    v = Field{Function}((x,y,z) -> sin(x) * cos(y), (x,y,z) -> z*cos(x) * sin(y)^2, (x,y,z) -> sin(x) * cos(y) * sin(z))
    @test dimension(v) == 3
    @test codimension(v) == 3
end

@testset "Construction of Mappings" begin
    m = Mapping{Function}((x,y,z) -> sin(x) * cos(y), (x,y,z) -> z*cos(x) * sin(y)^2)
    @test dimension(m) == 3
    @test codimension(m) == 2
end

# function used to check behavior and results from optimized routines in src
function eval_function_on_grid(f, X...)
    y = zeros(map(length, X)...)
    for (k, x) in enumerate(Iterators.product(X...))
        y[k] = f(x...)
    end
    return y
end

@testset "Mapping evaluation" begin
    f = Mapping{Function}((x,y,z) -> x^2 * y + z^3, (x,y,z) -> x^4 + y * z^3)
    X = (LinRange(0.0,1.0,4), LinRange(2.0,4.0,5), LinRange(3.0,6.0,3))
    Y = [zeros(map(length,X)) for i in 1:2]
    @evaluate! Y = f(X)
    @test Y[1] == eval_function_on_grid(f[1], X...)
    @test Y[2] == eval_function_on_grid(f[2], X...)
end

end
