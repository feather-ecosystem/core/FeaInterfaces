# SafeTestsets does not support macros. Hence, here we
# use a module to create a safe environment
module EvaluationTest

using Test
using FeaInterfaces

import FeaInterfaces: evalkernel!

@testset "Grid evaluation 1d" begin
    X = [0.0:1.0:3.0...]
    n = length(X)
    Y = zeros(n...)
    Z = zeros(n...)
    f = x -> sin(x) * x^2 + x

    @evaluate! Y += f(X)
    evalkernel!(Val(:(+=)), Z, X, f)
    @test Y == Z

    @evaluate! Y *= f(X)
    evalkernel!(Val(:(*=)), Z, X, f)
    @test Y == Z

    @evaluate! Y -= f(X)
    evalkernel!(Val(:(-=)), Z, X, f)
    @test Y == Z

    @evaluate! Y = f(X)
    evalkernel!(Val(:(=)), Z, X, f)
    @test Y == Z
end

@testset "Grid evaluation 3d" begin
    X = ([0.0:1.0:3.0...], [0.0:1.0:4.0...], [0.0:1.0:5.0...])
    n = map(length, X)
    Y = zeros(n...)
    Z = zeros(n...)
    h(x,y,z) = sin(x) * y^2 + z

    @evaluate! Y += h(X)
    evalkernel!(Val(:(+=)), Z, X, h)
    @test Y == Z

    @evaluate! Y *= h(X)
    evalkernel!(Val(:(*=)), Z, X, h)
    @test Y == Z

    @evaluate! Y -= h(X)
    evalkernel!(Val(:(-=)), Z, X, h)
    @test Y == Z

    @evaluate! Y = h(X)
    evalkernel!(Val(:(=)), Z, X, h)
    @test Y == Z
end

# function used to check behavior and results from optimized routines in src
function eval_function_on_grid(f, X...)
    y = zeros(map(length, X)...)
    for (k, x) in enumerate(Iterators.product(X...))
        y[k] = f(x...)
    end
    return y
end

@testset "function evaluation on grid" begin
    g = (x,y,z) -> x^2 * y + z^3
    X = (LinRange(0.0,1.0,4), LinRange(2.0,4.0,5), LinRange(3.0,6.0,3))
    Y = zeros(map(length, X))
    @evaluate! Y = g(X)
    @test Y[:,:,:,1] == eval_function_on_grid(g, X...)
end

end # module
