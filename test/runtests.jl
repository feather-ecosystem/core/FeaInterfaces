using Test

tests = [
            "base",
            "linalg",
            "spaces",
            "evaluation",
            "mappings",
            "operators",
            "projection",
            "refinement",
            "quadrature",
        ]

@testset "FeaInterfaces" begin
    for t in tests
        fp = joinpath(dirname(@__FILE__), "$t.jl")
        println("$fp ...")
        include(fp)
    end
end # @testset
