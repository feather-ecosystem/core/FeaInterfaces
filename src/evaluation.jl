export @evaluate!, evalkernel!
export EvaluationCache, update!, isinitialized

function evalkernel! end

"""
    @evaluate! Y  = f(x)
    @evaluate! Y += f(x)
    @evaluate! Y -= f(x)
    @evaluate! Y *= f(x)
    @evaluate! Y /= f(x)

Fast evaluation routine for evaluating a function on a grid  ``x ...``. The
result is stored in array ``Y`` and needs to be preallocated of the correct
size.
"""
macro evaluate!(ex)
    local op = esc(Val(ex.head))
    local Y = esc(ex.args[1])
    local f = esc(ex.args[2].args[1])
    local X = esc(ex.args[2].args[2])
    return :(evalkernel!($op, $Y, $X, $f))
end

# fast evaluation of standard Julia functions on a Cartesian grid of points
# defined by a tuple or CartesianProduct of vectors
using Base.Cartesian
for op in [:(=), :(+=), :(-=), :(*=), :(/=)]
    local S = Val{op}

    # 1d special case
    local kernel_expression = Expr(op, Expr(:ref, :Y, :k), Expr(:call, :func, :x))
    @eval function evalkernel!(::$S, Y::AbstractVector, X, func::Function)
        for k in 1:length(X)
            x = X[k]
            $kernel_expression
        end
        return Y
    end

    # 2d to 4d with Cartesian product points
    for Dim in 2:4
        local kernel_expression = Expr(op, :(@nref $Dim Y i), :(@ncall $Dim func x))
        @eval function evalkernel!(::$S, Y::AbstractArray{T,$Dim}, X, func::Function) where T
            @nloops $Dim i Y d -> x_d = X[d][i_d] begin
                $kernel_expression
            end
            return Y
        end
    end
end

abstract type EvaluationCache{Dim} end

# Concrete subtypes of EvaluationCache have a mutable struct layout that
# looks like
#
# mutable struct MyEvaluationCache <: EvaluationCache{Dim}
#     func::F
#     basis::S
#     grid::T
#     isinit::Bool
#     function EvaluationCache(f::Function)
#         eval = new()
#         eval.func = f
#         eval.isinit = false
#         return eval
#     end
# end

isinitialized(eval::EvaluationCache) = eval.isinit
Base.ndims(::EvaluationCache{Dim}) where Dim = Dim

# A minimal implementation implements the following functionality
update!(eval::EvaluationCache, x) = throw(NotImplementedError("update! is not implemented."))
