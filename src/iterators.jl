export Boundary, num_boundary_components, boundary_element_type

struct Boundary{S}
    data::S
end

function num_boundary_components end
function boundary_element_type end

# the following functions need to be implemented in order to use the
# Boundary iterator
Base.length(b::Boundary) = num_boundary_components(b.data)
Base.eltype(b::Boundary) = boundary_element_type(b.data)

function Base.iterate(iter::Boundary)
    return boundary(iter.data, 1), 1
end

function Base.iterate(iter::Boundary, state)
    if state < length(iter)
        state+=1
        return boundary(iter.data, state), state
    end
end