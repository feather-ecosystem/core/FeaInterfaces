export l2_error,  l2_error_relative

"""
    l2_error(f, g; quadrule)

Compute the L^2 error between two general functions `f` and `g`, Any `f` and `g`
will do as long as `@cartesian` and `standard_quadrature_rule(f,g)` are implemented.
"""
function l2_error(f, g; quadrule::AbstractQuadratureRule=standard_quadrature_rule(f,g))
    F = zeros(size(quadrule))
    @evaluate! F = f(quadrule.x)
    @evaluate! F -= g(quadrule.x)
    F .*= F
    return sqrt(contract(F, quadrule.w))
end

"""
    l2_error_relative(f, g; quadrule)

Compute the L^2 error between two general functions `f` and `g`, Any `f` and `g`
will do as long as `@cartesian` and `standard_quadrature_rule(f,g)` are implemented.
"""
function l2_error_relative(f, g; quadrule::AbstractQuadratureRule=standard_quadrature_rule(f,g))
    F = zeros(size(quadrule))
    @evaluate! F = f(quadrule.x)
    F .*= F
    r = sqrt(contract(F, quadrule.w)) 

    F ./= F
    @evaluate! F -= g(quadrule.x)
    F .*= F
    e = sqrt(contract(F, quadrule.w))

    return e / r
end
