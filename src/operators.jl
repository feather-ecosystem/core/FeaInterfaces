export Lazy, Gradient, Curl, Divergence, Jacobian, Hessian
export gradient, jacobian, hessian

# this function needs to be extended in order to use getindex on Lazy objects
function partial_derivative(f, ders)
    return (f, ders)
end

abstract type AbstractOperator end
struct Gradient <: AbstractOperator end
struct Curl <: AbstractOperator end
struct Divergence <: AbstractOperator end
struct Jacobian <: AbstractOperator end
struct Hessian <: AbstractOperator end
struct Composition <: AbstractOperator end

const GradOrJac = Union{Gradient,Jacobian}

"""
    y = ∇f(x)

Evaluate a `Lazy` gradient or jacobian as a functor. Requires implementation
of the `@evaluate!` macro for the specified input type.
"""
struct Lazy{S<:AbstractOperator, T<:AbstractMapping,N} <:AbstractArray{T,N}
    f::T
    function Lazy{S}(f::T) where {S<:AbstractOperator, T<:AbstractMapping}
        N = _ndims(S(), Val(dimension(f)), Val(codimension(f)))
        return new{S,T,N}(f)
    end
end

_ndims(::GradOrJac, dim, codim) = 2
_ndims(::Hessian, dim, codim) = 2

# implicit constructors
gradient(f::AbstractMapping) = Lazy{Gradient}(f)
jacobian(f::AbstractMapping) = Lazy{Jacobian}(f)
hessian(f::AbstractMapping)  = Lazy{Hessian}(f)

Base.size(f::Lazy{<:GradOrJac, <:AbstractMapping{T,Dim,Codim}}) where {T, Dim, Codim} = (Codim,Dim)
Base.size(f::Lazy{<:Hessian, <:AbstractMapping{T,Dim,1}}) where {T, Dim} = (Dim,Dim)

# cartesian indexing
@inline function Base.getindex(∇f::Lazy{<:GradOrJac, <:AbstractMapping{T,1}}, i::Int) where {T}
    checkbounds(∇f, i)
    return partial_derivative(∇f.f[i], 1)
end

@inline function Base.getindex(∇f::Lazy{<:GradOrJac, <:AbstractMapping{T,Dim}}, i::Int, j::Int, k...) where {T,Dim}
    checkbounds(∇f, i, j, k...)
    return partial_derivative(∇f.f[i], ntuple(l -> Int(l==j), Dim))
end

@inline function Base.getindex(Δf::Lazy{<:Hessian, <:AbstractMapping{T,Dim}}, i::Int, j::Int, k...) where {T,Dim}
    checkbounds(Δf, i, j, k...)
    return partial_derivative(Δf.f[1], ntuple(l -> Int(l==i)+Int(l==j), Dim))
end

# linear indexing
@inline function Base.getindex(J::Lazy, k::Int)
    return getindex(J, CartesianIndices(size(J))[k].I...)
end

@inline function (∇f::Lazy{<:GradOrJac,<:AbstractMapping{S,Dim,Codim}})(x) where {S,Dim,Codim}
    T = eltype(x)
    J = [Array{T,Dim}(undef, size(x)) for i in 1:Codim, j in 1:Dim]
    @evaluate! J = ∇f(x)
    return J
end

# fall-back method that evaluates component-wise
@inline function evalkernel!(op::Val{OP}, ∇Y, X, G::Lazy) where {OP}
    @assert length(∇Y) == length(G) "Allocate array of correct size."
    for j in 1:length(G)
        evalkernel!(op, ∇Y[j], X, G[j])
    end
end
