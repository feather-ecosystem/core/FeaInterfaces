export FeatherMapping, Spline, AbstractMapping, Field, Mapping

"""
    y = f(x)

Evaluate an `AbstractMapping` as a functor. Requires implementation
of the `@evaluate!` macro for the specified input type.
"""
abstract type FeatherMapping{Dim,Codim} end

@inline dimension(::FeatherMapping{Dim}) where Dim = Dim
@inline codimension(::FeatherMapping{Dim,Codim}) where {Dim,Codim}= Codim

abstract type Spline{Dim,T} <: FeatherMapping{Dim,1} end
@inline Base.eltype(::Spline{Dim, T}) where {Dim,T} = T
nargs(spline::Spline) = dimension(spline)

abstract type AbstractMapping{T,Dim,Codim} <: FeatherMapping{Dim,Codim} end

Base.length(f::AbstractMapping) = length(f.data)
Base.eltype(::AbstractMapping{T}) where T = T

@inline Base.iterate(F::AbstractMapping, state=1) = iterate(getfield(F, :data), state)

struct Field{T,Dim,Codim,S<:Tuple} <: AbstractMapping{T,Dim,Codim}
    data::S
    function Field{T}(args::S) where {T,S<:Tuple}
        check_mapping_arguments(Val(T), args)
        dim = dimension(Val(T), args)
        codim = codimension(Val(T), args)
        return new{T,dim,codim,S}(args)
    end
end

Field{T}(args...) where T = Field{T}(args)

struct Mapping{T,Dim,Codim,S<:Tuple} <: AbstractMapping{T,Dim,Codim}
    data::S
    function Mapping{T}(args::S) where {T,S<:Tuple}
        check_mapping_arguments(Val(T), args)
        dim = dimension(Val(T), args)
        codim = codimension(Val(T), args)
        return new{T,dim,codim,S}(args)
    end
end

Mapping{T}(args...) where T = Mapping{T}(args)

@inline function Mapping(T::Type{<:FeatherMapping}, space; codimension::Int=1)
    x = T(space)
    y = ntuple(k -> similar(x), codimension-1)
    return Mapping{T}((x, y...))
end

@inline function Field(T::Type{<:FeatherMapping}, space; codimension::Int=1)
    x = T(space)
    y = ntuple(k -> similar(x), codimension-1)
    return Field{T}((x, y...))
end

# fall-back method
function check_mapping_arguments(::Val{T}, args) where T end

# fall-back method
function dimension(::Val{T}, args) where T
    dim = nargs(first(args))
    for f in args
        nargs(f) != dim && throw(ArgumentError("Functors require the same number of input arguments."))
    end
    return dim
end

# fall-back method
function codimension(::Val{T}, args) where T
    return length(args)
end

# fall-back method
function Base.getproperty(f::AbstractMapping, s::Symbol)
    s === :x && return getfield(f, :data)
    return getfield(f, s)
end

# fall-back method
function Base.propertynames(f::AbstractMapping)
    return (:x,)
end

# fall-back method that enables use of functions with single method implementation
# check needs to be added that there is only one method implementation.
Base.@pure function nargs(f::Function)::Int
    m = first(methods(f))
    return m.nargs - 1
end

Base.getindex(f::AbstractMapping, i...) = getindex(getfield(f,:data), i...)

@inline function (F::AbstractMapping{T,Dim,1})(x) where {T, Dim}
    return F[1](x)
end
@inline function (F::AbstractMapping)(x)
    return ntuple(k -> F[k](x), codimension(F))
end

@inline function evalkernel!(op::Val{OP}, Y, X, mapping::AbstractMapping{S,Dim,1}) where {OP,S,Dim}
    evalkernel!(op, Y, X, mapping[1])
end

@inline function evalkernel!(op::Val{OP}, Y, X, mapping::AbstractMapping) where {OP}
    @assert length(Y) == codimension(mapping) "Allocate array of correct size."
    for k in 1:codimension(mapping)
        evalkernel!(op, Y[k], X, mapping[k])
    end
end

"""
    boundary(F, args...; kwargs...)
    ∂(F, args...; kwargs...)

Return the boundary components of a mapping.
"""
function boundary(F::Mapping{T}, args...; kwargs...) where T
    return Mapping{T}(ntuple(k -> boundary(F[k], args...; kwargs...), codimension(F)))
end

function boundary(F::Field{T}, args...; kwargs...) where T
    return Field{T}(ntuple(k -> boundary(F[k], args...; kwargs...), codimension(F)))
end
