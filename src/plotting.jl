export visualization_grid

using RecipesBase

function visualization_grid end

##region Plotting of quadrature rules
@recipe function f(Q::AbstractQuadratureRule; seriestype=:qpoints)
    return Q, Val(seriestype)
end

@recipe function f(Q::AbstractQuadratureRule{1}, ::Val{:qpoints})
    return Q.x, zeros(length(Q.x))
end

@recipe function f(Q::AbstractQuadratureRule{1}, ::Val{:qweights})
    return Q.x, Q.w
end

# series recipe 'quadrature-points'
@recipe function f(::Type{Val{:qpoints}}, x, y, z)
    label       --> "quadrature rule"
    seriestype  := :scatter
    markershape --> :circle
    markercolor --> :black
    markersize  --> 2
end

# series recipe 'quadrature-weights'
@recipe function f(::Type{Val{:qweights}}, x, y, z)
    label       --> ""
    seriestype  :=  :stem
    linewidth   --> 1
    linecolor   --> :black
    marker      --> :circle
    markersize  --> 3
    markercolor --> :black
end
##endregion

##region Plotting of curves

@recipe function f(fun::FeatherMapping{1}; seriestype=:path)
    return fun, Val(seriestype)
end

# plot recipe 1-dimensional function
@recipe function f(fun::FeatherMapping{1,1}, ::Val; density=10)
    xguide --> "x"
    yguide --> "y"

    linewidth   --> 1
    linecolor   --> :black
    seriestype  --> :path

    x = visualization_grid(fun, density)
    y = fun(x)
    return x, y
end

# plot recipe spatial curves
@recipe function f(curve::FeatherMapping{1}, ::Val; density=10)
    label -->"curve",
    linewidth   --> 1
    linecolor   --> :black
    seriestype  --> :path

    x = visualization_grid(curve[1], density)
    y = curve(x)
    return y
end

##endregion

##region Plotting of surfaces

@recipe function f(mapping::FeatherMapping{2}; seriestype=:mapping)
    return mapping, Val(seriestype)
end

@recipe function f(mapping::FeatherMapping{3}; seriestype=:mapping)
    return mapping, Val(seriestype)
end

@recipe function f(mapping::FeatherMapping{2,1}, ::Val; density=5)
    seriestype := :surface
    fillcolor  --> :lightgreen
    alpha  --> 0.7
    legend --> :none

    x = visualization_grid(mapping, density)
    y = mapping(x)
    return x[1], x[2], y
end

@recipe function f(mapping::FeatherMapping{2,3}, ::Val; density=5)
    seriestype := :surface
    fillcolor  --> :lightblue
    alpha  --> 0.7
    legend --> :none

    x = visualization_grid(mapping, density)
    y = mapping(x)
    return y[1], y[2], y[3]
end

@recipe function f(mapping::FeatherMapping{3,3}, st::Val; density=5)
    for b in Boundary(mapping)
        @series begin
            density --> density
            b
        end
    end
end

##endregion
