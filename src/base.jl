# define here any functions that should allow overloading inside
# the Feather eco-system

export Degree, Regularity, Dimension, Interval
export boundary, ∂
export Even, Odd, EvenOrOdd, select_even_or_odd

export apply, collect!
export dimension, codimension, order, degree, orientation, direction
export partial_derivative, gradient, curl, divergence, jacobian, hessian
export contract, contract!
export nargs, get_elements
export unzip
export NotImplementedError

"""
    Degree

Polynomial degree (typeallias of Integer).
"""
const Degree = Integer

"""
    Regularity

Regularity of the B-spline basis (typeallias of Integer).
"""
const Regularity = Integer

"""
    Dimension

Dimension (typeallias of Integer).
"""
const Dimension = Integer

struct Interval{T}
    a::T
    b::T
end
Base.length(I::Interval) = I.b-I.a

function (I::Interval)(x::T) where {T<:Real}
    y = I.a <= x <= I.b ? x : T(Inf)
    return y
end

apply(args...) = map(args...)

function collect! end

function dimension end
function codimension end
function order end
function degree end
function orientation end
function direction end
function update! end
function isinitialized end
function contract end
function contract! end

contract(a::AbstractVector, b::AbstractVector) = dot(a,b)

function boundary end
∂(args...) = boundary(args...)

abstract type EvenOrOdd  end
struct Even <: EvenOrOdd end
struct Odd  <: EvenOrOdd end

select_even_or_odd(m) = iseven(m) ? Even() : Odd()

function partial_derivative end
function gradient end
function curl end
function divergence end
function jacobian end
function hessian end

function nargs end
function get_elements end

unzip(c::NTuple{N,T}) where {N,T} = map(x -> getfield.(c, x), fieldnames(T))
unzip(c::Vector{T}) where {T} = map(x -> getfield.(c, x), fieldnames(T))

struct NotImplementedError <: Exception
    msg::AbstractString
end
NotImplementedError() = NotImplementedError("Not implemented.")

function Base.showerror(io::IO, err::NotImplementedError)
    print(io, "NotImplementedError: ")
    print(io, err.msg)
end
