module FeaInterfaces

    include("base.jl")
    include("linalg.jl")
    include("spaces.jl")
    include("evaluation.jl")
    include("mappings.jl")
    include("operators.jl")
    include("projection.jl")
    include("refinement.jl")
    include("quadrature.jl")
    include("postprocessing.jl")
    include("iterators.jl")
    include("plotting.jl")

end # module
