export AbstractQuadratureRule, QuadratureRule, GaussianRule, Legendre, Lobatto
export dimension_quadrature_rule, standard_quadrature_rule
export TargetSpace

export pre_allocate_rhs_arrays, pre_allocate_element_arrays, sumfact!

abstract type AbstractQuadratureRule{Dim} end

Base.length(Q::AbstractQuadratureRule) = length(Q.x)
Base.size(Q::AbstractQuadratureRule) = size(Q.x)
Base.size(Q::AbstractQuadratureRule, k) = size(Q.x, k)
Base.copy(Q::AbstractQuadratureRule) = Base.deepcopy(Q)

# back-up method
dimension_quadrature_rule(x::AbstractVector, w) = 1
dimension_quadrature_rule(x::NTuple{Dim}, w) where {Dim} = Dim


function standard_quadrature_rule end

struct QuadratureRule{Dim,X,W} <: AbstractQuadratureRule{Dim}
    x::X
    w::W
    function QuadratureRule(x::X, w::W) where {X,W}
        Dim = dimension_quadrature_rule(x, w)
        return new{Dim,X,W}(x, w)
    end
end

# traits
abstract type GaussianRule end
struct Legendre <: GaussianRule end
struct Lobatto <: GaussianRule end