export AbstractProjection, AbstractInterpolation
export project!, GalerkinProjection, Interpolation, QuasiInterpolation

abstract type AbstractProjection end
abstract type AbstractInterpolation <: AbstractProjection end

struct GalerkinProjection <:AbstractProjection end
struct Interpolation <: AbstractInterpolation end
struct QuasiInterpolation <: AbstractInterpolation end

function project! end

project!(g, s::AbstractMapping, method::AbstractProjection) = map((gₖ,sₖ) -> project!(gₖ, sₖ, method), g, s)
