# FeaInterfaces

[![pipeline status](https://gitlab.com/feather-ecosystem/FeaInterfaces/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/FeaInterfaces/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/FeaInterfaces/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/FeaInterfaces/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/FeaInterfaces/branch/master/graph/badge.svg?token=ESA77DBG7I)](https://codecov.io/gl/feather-ecosystem:core/FeaInterfaces)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/FeaInterfaces/)


